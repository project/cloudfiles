Cloudfiles
==========

Allows Drupal files to be stored in Rackspace Cloudfiles.

Installation
------------

 1. Download and place the 'cloudfiles' module into your sites/all/modules directory

 2. Download the Cloud Files PHP API from Rackspace's account on GitHub:

    a. Go to https://github.com/rackspace/php-cloudfiles

	b. Click "Downloads" and select .tar.gz or .zip (which ever you know how to extract!)

	c. Save to disk!

 3. Extract the file you download into sites/all/libraries and rename the directory: "cloudfiles"

 4. Enable the 'cloudfiles' module in your Drupal installing, either:

      http://YOUR_SITE_NAME_HERE/admin/build/modules

	    - or, via drush -
	
	  drush en cloudfiles

Copyright
---------

Copyright (C) 2012 David Snopek

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

