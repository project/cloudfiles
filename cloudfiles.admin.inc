<?php

/**
 * @file
 * Admin pages.
 */

/**
 * Settings page
 */
function cloudfiles_admin_settings() {
  $form = array();

  $node_types = array();
  foreach(node_get_types() as $type){
    $node_types[$type->type] = $type->name;
  }

  $form['cloudfiles_excluded_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Excluded content types'),
    '#description' => t('Select the node types whose files you <em>don\'t</em> want sent to cloudfiles'),
    '#options' => $node_types,
    '#default_value' => variable_get('cloudfiles_excluded_node_types', array()),
  );

  $max_upload_options = array();
  for ($i = 1; $i <= 10; $i++) {
    $max_upload_options[$i*10] = $i*10;
  }

  $form['cloudfiles_cron_upload_max'] = array(
    '#type' => 'select',
    '#title' => t('Cron upload max'),
    '#description' => t('The maximum number of files to upload per cron.'),
    '#options' => $max_upload_options,
    '#default_value' => variable_get('cloudfiles_cron_upload_max', 10),
  );

  $form['cloudfiles_upload_immediately'] = array(
    '#type' => 'checkbox',
    '#title' => t('Upload immediately'),
    '#description' => t('Normally we upload to cloudfiles during cron.  Check this checkbox to upload them immediately.'),
    '#default_value' => variable_get('cloudfiles_upload_immediately', FALSE),
  );

  $form['storage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Storage'),
    '#collapsible' => TRUE,
    '#collapsed' => variable_get('cloudfiles_user', '') != '' && variable_get('cloudfiles_key', '') != '' && variable_get('cloudfiles_container', '') != '',
  );
  $form['storage']['cloudfiles_user'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cloudfiles_user', ''),
  );
  $form['storage']['cloudfiles_key'] = array(
    '#title' => t('API key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cloudfiles_key', ''),
  );
  $form['storage']['cloudfiles_container'] = array(
    '#title' => t('Container'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cloudfiles_container', ''),
  );
  $form['storage']['cloudfiles_servicenet'] = array(
    '#title' => t('Use the servicenet'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cloudfiles_servicenet', FALSE),
    '#description' => t('If you are running on Rackspace Cloud, you can use the "servicenet" which means you connect via the internal network.  This will give you an incredible performance boost!'),
  );

  return system_settings_form($form);
}

