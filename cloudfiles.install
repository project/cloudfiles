<?php

/**
 * Implementation of hook_schema().
 */
function cloudfiles_schema() {
  $schema = array();

  $schema['cloudfiles_upload_queue'] = array(
    'description' => 'A list of files to upload to cloudfiles later',
    'fields' => array(
      'fid' => array(
        'description' => 'The name of the handler this file belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'nid' => array(
        'description' => 'The node (if any) that this file is attached to.',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'timestamp' => array(
        'description' => 'UNIX timestamp for when the file was added to the queue.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0
      ),
    ),
    'indexes' => array(
      'nid'       => array('nid'),
      'timestamp' => array('timestamp'),
    ),
    'primary key' => array('fid'),
  );

  $schema['cloudfiles_comments'] = array(
    'description' => t('Store information about uploaded files that are connected to comments'),
    'fields' => array(
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Primary Key: The {files}.fid.'),
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.nid of the comment the uploaded files is associated with.'),
      ),
      'cid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {comment}.cid associated with the uploaded file.'),
      ),
      'filepath' => array(
        'description' => t('Path of the file relative to Drupal root.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('fid'),
    'indexes' => array(
      'cid_fid' => array('cid', 'fid'),
      'nid' => array('nid'),
    ),
  );

  $schema['cloudfiles_nodes'] = array(
    'description' => t('Store information about uploaded files that are connected to nodes via the upload module'),
    'fields' => array(
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {files}.fid.'),
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.nid of the node the uploaded files is associated with.'),
      ),
      'vid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.vid of the node the uploaded files is associated with.'),
      ),
      'filepath' => array(
        'description' => t('Path of the file relative to Drupal root.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('vid', 'fid'),
    'indexes' => array(
      'fid' => array('fid'),
      'nid' => array('nid'),
    ),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function cloudfiles_install() {
  drupal_install_schema('cloudfiles');

  // make sure that we come after all modules that manipulate the uploaded files,
  // particularly 'filefield_paths' and 'uploadpath'.
  db_query("UPDATE {system} SET weight = 999 WHERE name = 'cloudfiles'");
}

/**
 * Implementation of hook_uninstall().
 */
function cloudfiles_uninstall() {
  drupal_uninstall_schema('cloudfiles');
}

/**
 ** UPDATE FUNCTIONS:
 **/

/**
 * Add the new 'cloudfiles_comments' table.
 */
function cloudfiles_update_6001() {
  $schema = array();
  $schema['cloudfiles_comments'] = array(
    'description' => t('Store information about uploaded files that are connected to comments'),
    'fields' => array(
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Primary Key: The {files}.fid.'),
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.nid of the comment the uploaded files is associated with.'),
      ),
      'cid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {comment}.cid associated with the uploaded file.'),
      ),
      'filepath' => array(
        'description' => t('Path of the file relative to Drupal root.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('fid'),
    'indexes' => array(
      'cid_fid' => array('cid', 'fid'),
      'nid' => array('nid'),
    ),
  );
  
  $ret = array();
  foreach ($schema as $name => $table) {
    db_create_table($ret, $name, $table);
  }
  return $ret;
}

/**
 * Add the new 'cloudfiles_nodes' table.
 */
function cloudfiles_update_6002() {
  $schema = array();
  $schema['cloudfiles_nodes'] = array(
    'description' => t('Store information about uploaded files that are connected to nodes via the upload module'),
    'fields' => array(
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {files}.fid.'),
      ),
      'nid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.nid of the node the uploaded files is associated with.'),
      ),
      'vid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The {node}.vid of the node the uploaded files is associated with.'),
      ),
      'filepath' => array(
        'description' => t('Path of the file relative to Drupal root.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('vid', 'fid'),
    'indexes' => array(
      'fid' => array('fid'),
      'nid' => array('nid'),
    ),
  );
  
  $ret = array();
  foreach ($schema as $name => $table) {
    db_create_table($ret, $name, $table);
  }
  return $ret;
}

